package com.julie.room_db

import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

class MainActivity : AppCompatActivity(), UserRecyclerViewAdapter.ItemClickListener {

    private val viewModel: UserViewModel by viewModels()

    private lateinit var adapter: UserRecyclerViewAdapter
    var clickedUser: User? = null
    var isUpdate = false

    private lateinit var username: TextInputEditText
    private lateinit var email: TextInputEditText
    private lateinit var submitBtn: Button
    private lateinit var usersRv: RecyclerView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val usernameLayout = findViewById<TextInputLayout>(R.id.user_name_layout)
        val emailLayout = findViewById<TextInputLayout>(R.id.email_layout)
        username = findViewById(R.id.user_name)
        email = findViewById(R.id.email)
        submitBtn = findViewById(R.id.submit_btn)
        usersRv = findViewById(R.id.user_rv)
        val text = findViewById<TextView>(R.id.no_users)

        setupRecyclerView()

        //Every time, the data change, this code will run and update the list, if no data exist, it will display a text saying no users
        viewModel.users?.observe(this, {

            if (it != null && it.isNotEmpty()) {
                usersRv.visibility = View.VISIBLE
                text.visibility = View.GONE
                adapter.setItems(it)
            } else {
                usersRv.visibility = View.GONE
                text.visibility = View.VISIBLE
            }

            Toast.makeText(this, it.toString(), Toast.LENGTH_LONG).show()

        })


        submitBtn.setOnClickListener {
            val usernameText = username.text.toString().trim()
            val emailText = email.text.toString().trim()

            if (TextUtils.isEmpty(usernameText)) {
                username.requestFocus()
                usernameLayout.error = "Enter username"
            } else if (TextUtils.isEmpty(emailText)) {
                email.requestFocus()
                emailLayout.error = "Enter email"
            } else {

                if (isUpdate) {
                    val user = User(clickedUser?.userId, usernameText, emailText)
                    viewModel.updateUsers(user)

                    submitBtn.text = getString(R.string.submit)
                    isUpdate = false

                } else {
                    val user = User( userName = usernameText, email = emailText)
                    viewModel.insertUsers(user)
                }

                email.setText("")
                username.setText("")

            }
        }
    }

    private fun setupRecyclerView() {
        adapter = UserRecyclerViewAdapter(this)
        usersRv.adapter = adapter
    }

    override fun clickUser(user: User) {
        isUpdate = true
        clickedUser = user

        username.setText(user.userName)
        email.setText(user.email)
        submitBtn.text = getString(R.string.update)
    }
}