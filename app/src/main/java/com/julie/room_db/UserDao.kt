package com.julie.room_db

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUser(users: User)

    //Since we are returning a live data no need to wrap in suspend function
    @Query("Select * from user")
    fun gelAllUsers(): LiveData<List<User>>

    @Update
    suspend fun updateUser(users: User)

    @Delete
    suspend fun deleteUser(users: User)

}