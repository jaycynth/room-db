package com.julie.room_db

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class UserRecyclerViewAdapter(private val listener: ItemClickListener) :
    RecyclerView.Adapter<UserRecyclerViewAdapter.UsersViewHolder>() {

    private val users = ArrayList<User>()

    //Extract this separately so that we don't have to rebuild the adapter every time the data changes
    fun setItems(users: List<User>) {
        this.users.clear()
        this.users.addAll(users)
    }

    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): UserRecyclerViewAdapter.UsersViewHolder {
        // Return a new holder instance
        return UsersViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.user_item_layout, parent, false)
        )
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(
        viewHolder: UserRecyclerViewAdapter.UsersViewHolder,
        position: Int
    ) {
        // Get the data model based on position
        val user = users[position]
        // Set item views based on your views and data model
        viewHolder.username.text = user.userName
        viewHolder.email.text = user.email
        viewHolder.layout.setOnClickListener {
            listener.clickUser(user)
        }

    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return users.size
    }

    inner class UsersViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val username: TextView = itemView.findViewById(R.id.username_item)
        val email: TextView = itemView.findViewById(R.id.email_item)
        val layout: LinearLayout = itemView.findViewById(R.id.item_layout)
    }

    interface ItemClickListener {
        fun clickUser(user: User)
    }
}