package com.julie.room_db

import android.content.Context

class UserRepo(context:Context) {
    var db = AppDatabase.getInstance(context.applicationContext)
    private val userDao = db?.userDao()


    suspend fun insertUser(users: User){
        userDao?.insertUser(users)
    }

    suspend fun updateUser(users: User){
        userDao?.updateUser(users)
    }

    fun getUsers() = userDao?.gelAllUsers()
}