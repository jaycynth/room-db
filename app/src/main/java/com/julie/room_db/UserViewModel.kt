package com.julie.room_db

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class UserViewModel(app: Application) : AndroidViewModel(app) {

    private val userRepo = UserRepo(app.applicationContext)

    val users = userRepo.getUsers()

    fun insertUsers(users: User) = viewModelScope.launch {
        userRepo.insertUser(users)
    }

    fun updateUsers(users: User) = viewModelScope.launch {
        userRepo.updateUser(users)
    }
}